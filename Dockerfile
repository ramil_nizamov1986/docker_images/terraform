FROM hashicorp/terraform:1.7.4
add .terraformrc ~/.terraformrc
RUN apk --update --no-cache add jq
